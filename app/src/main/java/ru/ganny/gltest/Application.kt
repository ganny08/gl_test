package ru.ganny.gltest

import android.app.Application
import com.vk.api.sdk.VK

class GlApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        VK.initialize(this)
        com.vk.api.sdk.utils.VKUtils.getCertificateFingerprint(this, this.packageName)?.toList()?.toLogLn()
    }
}