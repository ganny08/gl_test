package ru.ganny.gltest.repository

import androidx.lifecycle.MutableLiveData
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.*
import ru.ganny.gltest.model.Profile
import ru.ganny.gltest.model.VkUser
import ru.ganny.gltest.provider.VkProvider
import ru.ganny.gltest.toLog
import java.lang.Exception

object Repository {
    var accessToken: VKAccessToken? = null
    val userList = MutableLiveData<List<VkUser>>()

    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + Dispatchers.IO)

    fun searchUsersAsync(query: String) {
        val request = VkProvider.VkUsersRequest(query)
        scope.launch(Dispatchers.IO) {
            try {
                val result = VK.executeSync(request)
                userList.postValue(result)
            } catch (ex: Exception) {
                ex.toLog()
            }
        }
    }

    suspend fun loadProfileAsync(id: Int): Profile? {
        val request = VkProvider.VkProfileRequest(id)
        return scope.async(Dispatchers.IO) {
            return@async try {
                VK.executeSync(request)
            } catch (ex: Exception) {
                ex.toLog()
                null
            }
        }.await()
    }

}