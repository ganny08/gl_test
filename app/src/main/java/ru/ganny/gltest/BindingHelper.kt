package ru.ganny.gltest

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class BindingHelper {
    companion object {
        @JvmStatic
        @BindingAdapter("bind:img")
        fun loadImg(view: ImageView, path: String?) {
            Glide.with(view)
                .load(path)
                .apply(RequestOptions.circleCropTransform())
                .into(view)
        }

        @JvmStatic
        @BindingAdapter("bind:adapter")
        fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
            view.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("bind:sex")
        fun setSex(view: TextView, sex: Int) {
            try {
                view.text = view.resources.getStringArray(R.array.sex)[sex]
            }catch (ex: IndexOutOfBoundsException) {
                view.text = view.resources.getStringArray(R.array.sex)[0]
            }
        }

        @JvmStatic
        @BindingAdapter("bind:relation")
        fun setRelation(view: TextView, relation: Int) {
            try {
                view.text = view.resources.getStringArray(R.array.relation)[relation]
            }catch (ex: IndexOutOfBoundsException) {
                view.text = view.resources.getStringArray(R.array.relation)[0]
            }
        }
    }
}