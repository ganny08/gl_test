package ru.ganny.gltest

import android.util.Log

private const val TAG = "gl_test"

fun Any?.toLog(prefix: String = "", methodName: String? = null, tag: String = TAG) {
    var tempMethodName = methodName
    try {
        tempMethodName = methodName ?: Thread.currentThread().stackTrace[4].methodName
    } catch (e: Exception){}
    Log.d(tag,
        "[${String.format("%5d", Thread.currentThread().id)}] [${String.format("%-15s", tempMethodName)}] $prefix: ${this}")
}

fun Throwable.toLog(prefix: String = "", methodName: String? = null, tag: String = TAG) {
    var tempMethodName = methodName
    try {
        tempMethodName = methodName ?: Thread.currentThread().stackTrace[4].methodName
    } catch (e: Exception){}
    this.message.toLog(prefix, tempMethodName, tag)
    this.printStackTrace()
}

fun <T> Iterable<T>.toLog(prefix: String = "", methodName: String? = null, tag: String = TAG) {
    var tempMethodName = methodName
    try {
        tempMethodName = methodName ?: Thread.currentThread().stackTrace[4].methodName
    } catch (e: Exception){}
    this.joinToString().toLog(prefix, tempMethodName, tag)
}

fun <T> Iterable<T>.toLogLn(prefix: String = "", methodName: String? = null, tag: String = TAG) {
    var tempMethodName = methodName
    try {
        tempMethodName = methodName ?: Thread.currentThread().stackTrace[4].methodName
    } catch (e: Exception){}
    "\n${this.joinToString("\n")}".toLog(prefix, tempMethodName, tag)
}