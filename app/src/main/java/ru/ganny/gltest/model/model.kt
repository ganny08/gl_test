package ru.ganny.gltest.model

import com.google.gson.annotations.SerializedName

data class VkUser(
    @SerializedName("id") val id: Int,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("photo_100") val photo: String
)

data class Profile(
    @SerializedName("id") val id: Int,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("photo_200") val photo: String,
    @SerializedName("sex")val sex: Int,
    @SerializedName("relation")val relation: Int,
    @SerializedName("nickname") val nickname: String,
    @SerializedName("screen_name") val screenName: String
) {
    fun getFullName(): String {
        return "$firstName $lastName"
    }
}