package ru.ganny.gltest.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.ganny.gltest.databinding.ListItemBinding
import ru.ganny.gltest.model.VkUser

class SearchAdapter(private val clickListener: (user: VkUser) -> Unit): ListAdapter<VkUser, UserViewHolder>(DiffCallback) {
    private companion object DiffCallback : DiffUtil.ItemCallback<VkUser>() {
        override fun areItemsTheSame(oldItem: VkUser, newItem: VkUser): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: VkUser, newItem: VkUser): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: VkUser, newItem: VkUser): Any? {
            return newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user)
        holder.itemView.setOnClickListener {
            clickListener(user)
        }
    }

    override fun onBindViewHolder(
        holder: UserViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        try {
            payloads.first { item -> item is VkUser }.also {
                holder.update(it as VkUser)
            }
        } catch (ex: NoSuchElementException) {
            onBindViewHolder(holder, position)
        }
    }
}

class UserViewHolder(private val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(user: VkUser) {
        binding.name = "${user.lastName} ${user.firstName}"
        binding.imgPath = user.photo
        binding.executePendingBindings()
    }

    fun update(user: VkUser) {
        binding.name = "${user.lastName} ${user.firstName}"
        binding.imgPath = user.photo
    }
}