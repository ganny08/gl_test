package ru.ganny.gltest.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.subjects.PublishSubject
import ru.ganny.gltest.model.VkUser
import ru.ganny.gltest.repository.Repository

class SearchViewModel : ViewModel() {
    val selectedUser = MutableLiveData<VkUser>()
    val userList = Repository.userList
    val adapter = SearchAdapter {
        selectedUser.postValue(it)
    }

    fun onCreate() {
        Repository.searchUsersAsync("")
    }

    fun searchUsers(query: String?) {
        Repository.searchUsersAsync(query ?: "")
    }

    fun resetSelectedUser() {
        selectedUser.postValue(null)
    }
}
