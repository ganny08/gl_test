package ru.ganny.gltest.ui.search

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import io.reactivex.disposables.CompositeDisposable
import ru.ganny.gltest.R
import ru.ganny.gltest.databinding.SearchFragmentBinding
import ru.ganny.gltest.toLog

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var viewModel: SearchViewModel
    private lateinit var binding: SearchFragmentBinding

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        viewModel.onCreate()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SearchFragmentBinding.inflate(inflater, container, false)
        binding.vm = viewModel
        binding.executePendingBindings()
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
        initUi()
    }

    private fun initSearchView() {
        binding.searchToolbar.menu.findItem(R.id.app_bar_search)?.let {
            val searchView = it.actionView as? SearchView
            searchView?.also {sv ->
                sv.setIconifiedByDefault(false)
                sv.requestFocus()
                sv.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        viewModel.searchUsers(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?) = false
                })
            }

        }
    }

    private fun getSearchQuery(): CharSequence? {
        return binding.searchToolbar.menu.findItem(R.id.app_bar_search)?.let {
            (it.actionView as? SearchView)?.query
        }
    }

    private fun initUi() {
        initSearchView()
        binding.searchSwipeContainer.setOnRefreshListener {
            viewModel.searchUsers(getSearchQuery()?.toString())
        }
    }

    private fun observeViewModel() {
        viewModel.userList.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty())
                binding.searchIsEmpty.visibility = View.VISIBLE
            else
                binding.searchIsEmpty.visibility = View.GONE
            viewModel.adapter.submitList(it)
            binding.searchSwipeContainer.isRefreshing = false
            binding.searchList.scrollToPosition(0)
        })
        viewModel.selectedUser.observe(viewLifecycleOwner, Observer {
            if(it == null)
                return@Observer
            findNavController().navigate(R.id.action_searchFragment_to_profileFragment, bundleOf( "id" to it.id))
            viewModel.resetSelectedUser()
        })
    }
}
