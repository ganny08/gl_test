package ru.ganny.gltest.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.ganny.gltest.model.Profile
import ru.ganny.gltest.repository.Repository
import ru.ganny.gltest.toLog
import java.lang.Exception

class ProfileViewModel : ViewModel() {

    val profile = MutableLiveData<Profile>()
    var userId: Int = 0
        set(value) {
            field = value
            viewModelScope.launch {
                profile.postValue(Repository.loadProfileAsync(value))
            }
        }
}
