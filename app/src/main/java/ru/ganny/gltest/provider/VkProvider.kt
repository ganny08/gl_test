package ru.ganny.gltest.provider

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vk.api.sdk.requests.VKRequest
import ru.ganny.gltest.model.Profile
import ru.ganny.gltest.model.VkUser
import ru.ganny.gltest.toLog

class VkProvider {
    companion object {
        private const val USER_SEARCH = "users.search"
        private const val USER_GET = "users.get"
        private const val QUERY_PARAM = "q"
        private const val PHOTO_100_FIELD = "photo_100"
        private const val PHOTO_200_FIELD = "photo_200"
        private const val SEX_FIELD = "sex"
        private const val RELATION_FIELD = "relation"
        private const val NICKNAME_FIELD = "nickname"
        private const val SCREE_NAME_FIELD = "screen_name"
        private const val USER_IDS = "user_ids"
        private const val FIELDS = "fields"
        private const val COUNT = "count"
        private const val OFFSET = "offset"
        private const val DEFAULT_COUNT = 25
    }

    internal class VkUsersRequest(
        query: String = "",
        count: Int = DEFAULT_COUNT,
        offset: Int = 0) : VKRequest<List<VkUser>>(USER_SEARCH) {

        init {
            addParam(QUERY_PARAM, query)
            addParam(FIELDS, PHOTO_100_FIELD)
            addParam(COUNT, count)
            addParam(OFFSET, offset)
        }

        override fun parse(response: String): List<VkUser> {
            response.toLog()
            val type = object: TypeToken<Answer<SearchResponse>>() {}.type
            val searchAnswer: Answer<SearchResponse> = Gson().fromJson(response, type)
            return searchAnswer.response.items
        }
    }

    internal class VkProfileRequest(
        userId: Int) : VKRequest<Profile>(USER_GET) {

        init {
            addParam(USER_IDS, userId)
            val params = arrayOf(PHOTO_200_FIELD, RELATION_FIELD, SEX_FIELD, NICKNAME_FIELD, SCREE_NAME_FIELD)
            addParam(FIELDS, params.joinToString(","))
        }

        override fun parse(response: String): Profile {
            response.toLog()
            val type = object: TypeToken<Answer<Array<Profile>>>() {}.type
            val profileAnswer: Answer<Array<Profile>> = Gson().fromJson(response, type)
            return profileAnswer.response[0]
        }
    }

    data class Answer<T>(
        val response: T
    )

    data class SearchResponse(
        val items: List<VkUser>
    )
}